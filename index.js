const math = require("@hardienpm/math");
const url = require("url");
const os = require("os");
const fs = require('fs');
const path = require("path");
const http = require('http');
const app = http.createServer();
const port = 8000;

var arrFiles = [];

function readDir(path) {
    let delimiter = (os.platform() === 'win32') ? "\\" : "/";
    files = fs.readdirSync(path);
    files.forEach(function (item) {
        state = fs.statSync(path + delimiter + item);
        if (state.isDirectory()) {
            localPath = path + delimiter + item;
            readDir(localPath);
        } else {
            // console.log(item);
            arrFiles.push(path + delimiter + item);
        }
    });
    return arrFiles;
}

app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});

app.on('request', function (request, response) {
    switch (request.method) {
        case 'GET': {
            if (url.parse(request.url).pathname === '/' || url.parse(request.url).pathname === '/index.html') {
                fs.readFile(path.join(__dirname, './index.html'), (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        response.writeHead(200);
                        response.end(data.toString());
                    }
                });
            } else if (url.parse(request.url).pathname === '/fsExplore') {
                // console.log(readDir(__dirname));
                response.writeHead(200);
                response.end(readDir(__dirname).join(','));
                arrFiles = [];
            }
            break;
        }
        case 'POST': {
            switch (url.parse(request.url).pathname) {
                case "/getMin": {
                    request.on('data', function (data) {
                        response.writeHead(200);
                        let a = Number(math.min(data.toString().split(',')));
                        if (a || a === 0) {
                            data.toString().split(',').forEach(element => {
                                if (element === '') {
                                    response.end("Enter right data");
                                };
                            });
                            response.end(math.min(data.toString().split(',')).toString());
                        } else {
                            response.end("Enter right data");
                        }
                    });
                    break;
                }
                default: {
                    response.writeHead(404);
                    response.end('Method is not available!!!');
                }
            }
            break;
        }
        default: {
            response.writeHead(404).end('Method is not available!!!');
        }
    }
});
